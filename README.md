# nlohmann_json-cocoapod

This project is unofficial to maintain a cocoapod for [nlohmann_json](https://github.com/nlohmann/json)

to add nlohmann/json to your iOS project add the following pod:

    pod 'nlohmann_json', '~>3.5.0'


# example

to run example simply run `pod install` first in the example project that
contains `Podfile` file. And open the xcworkspace file in xcode. You will
need to modify the code signing to your profile. Press play.
